var gpoGiphy = function () {
    this.SearchResults=undefined;
    this.key="oqAWQoklf0jYWPzapBVX4OWuN96HVXVS";
    this.caller=undefined;
};

gpoGiphy.prototype = {
    constructor: gpoGiphy,
    Init: function () {
        this.Load();
    },   
    Load:function() {
        var self=this;
        $(document).on('click', '.giphyIconComment', function () {
            self.DrawSearch($(this));
        });
        
        $(document).on('change', '.giphySearch input', function () {
            self.Search(this.value);
        });
        $(document).on('click', '.giphySingle img', function() {
            self.Insert(this);
        });
        $(document).on('click', '.closeGiphy', function() {
            $(".popupgiphy").remove();
        });
    },
    Insert:function(img) {
        var url=$(img).data("url");
        var id=$(img).data("id");
        var newComment=" hurz.me/p.php?id="+id;

        $(".popupgiphy").remove();

        this.caller.val(this.caller.val()+ newComment);
        this.caller.focus();
    },
    Search:function(query) {
        var queryItems=[];
        var singleItemDiv='<div class="giphySingle"><img data-id="[ID]" data-url="[URL]" src="[PREVIEW]"/></div>';
        var self=this;
        $.get("https://api.giphy.com/v1/gifs/search?q="+query+"&api_key="+self.key+"&limit=125", function(result) {
            var left="";
            var right="";
            var counter=0;    
            result.data.forEach(function(element) {
                if (element.images.original.width<425) return;                
                var singleItem=singleItemDiv.replace("[PREVIEW]",element.images.fixed_width_downsampled.webp).replace("[ID]", element.id).replace("[URL]",element.images.original.webp);
                if (counter++%2==0) {
                    left+=singleItem;
                } else {
                    right+=singleItem;
                }
            });

            var innerHtml='<div class="left">'+left+'</div><div class="right">'+right+'</div>';
            $(".popupgiphy .results").empty();
            $(".popupgiphy .results").append(innerHtml);
        }); 
    },
    DrawSearch:function($ce) {
        this.caller=$($ce.closest("c-wiz").find("textarea"));
        // TODO: Middle of window
        var searchHtmlWrapper='<div role="alertdialog" class="popupgiphy sVAYfc" style="left: 695px; top: 212px; width: 450px; height: 550px; transform-origin: 0px 0px 0px;" data-savescroll="0">[HEADER][SEARCH]</div>';
        var header='<div class="f0Eb9c"><div role="button" class="closeGiphy U26fgb mUbCce fKz7Od em1Z1 M9Bg4d" jslog="14679; track:JIbuQc" jscontroller="VXdfxd" jsaction="click:cOuCgd; mousedown:UX7yZ; mouseup:lbsD7e; mouseenter:tfO1Yc; mouseleave:JywGue;touchstart:p6p2H; touchmove:FwuNnf; touchend:yfqBxc(preventMouseEvents=true|preventDefault=true); touchcancel:JMtRjd;focus:AHmuwe; blur:O22p3e; contextmenu:mg9Pef;" jsshadow="" jsname="gQ2Xie" aria-label="Abbrechen" aria-disabled="false" tabindex="0"><div class="VTBa7b MbhUzd" jsname="ksKsZd"></div><content class="xjKiLb"><span style="top: -12px"><div class="XVzU0b"><svg viewBox="0 0 24 24" height="100%" width="100%"><path class="Ce1Y1c" d="M19 6.41l-1.41-1.41-5.59 5.59-5.59-5.59-1.41 1.41 5.59 5.59-5.59 5.59 1.41 1.41 5.59-5.59 5.59 5.59 1.41-1.41-5.59-5.59z"></path></svg></div></span></content></div></div>';
        var search='<div class="giphyLogo" ><img src="'+chrome.extension.getURL("./setup/images/giphy.png")+'"/></div><div class="giphySearch"><input  autofocus type="text" placeholder="'+chrome.i18n.getMessage("searchGif")+'"/><div class="results"</div></div>';
        $("body").append(searchHtmlWrapper.replace("[HEADER]",header).replace("[SEARCH]",search));
    },
    Draw: function($ce) {
        if ($ce.find('.giphyIconComment').length>0) return;
        icon='<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAABGCAMAAABG8BK2AAAAXVBMVEUAAAB1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXUHlRzdAAAAHnRSTlMAAQIJCx0fISUmJykqLC85OouVnaWqrbnV5u35+/18EPPMAAAAhElEQVRYw+3Quw7CMBBE0dnFhPcjJg6B2PP/n5kSBBKFSYOY021zpR1ARETkjwXyYhbJIyyyrLHKoS7D3tGSZ6Bl2WK5qMwkt0ieYJFlg6YyE8078gDvODbYlcqnrmkg7ykN5C31ZO02L5R5ZMKn+1czM22TxydZ28yd8f0bh4iIiHxrAm/eGx5vXY8EAAAAAElFTkSuQmCC" alt="search giphy">';
        var iconHtml='<div title="'+chrome.i18n.getMessage("searchGifLabel")+'" role="button" data-tooltip="'+chrome.i18n.getMessage("searchGifLabel") +'"   data-tooltip-position="bottom" data-tooltip-vertical-offset="-12" data-tooltip-horizontal-offset="0" class="giphyIconComment">[SVG]</div> ';

        var html=iconHtml.replace("[SVG]",icon);

        commentIcons=$ce.find("[jsname='p7VJJ']");
        if (commentIcons.length==0) return;

        $(commentIcons).append(html);
        $($.find(".giphyLogo input")).focus();
    },
    Dom: function ($ce) {
        this.Draw($ce);
    },
}