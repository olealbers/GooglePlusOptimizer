var gpoDiaspora = function () {};

gpoDiaspora.prototype = {
    constructor: gpoDiaspora,
    Init: function () {
        this.Load();
        this.ShowStartBlock();
    },   
    Load:function() {
        var self=this;
        $(document).on('click', '.diasporaShare', function () {
            self.ShareToDiaspora($(this));
        });
        
        $(document).on('click', '.hideDiaspora', function () {
            localStorage.setItem("diasporaDisabled",true);
            $(".diavortrag").remove();
            return false;
        });       
    },
    ShowStartBlock:function() {
        var disabledDiaspora=localStorage.getItem("diasporaDisabled");
        if (disabledDiaspora) return;

       

        $value=$("<div/>");
        var lang = chrome.i18n.getMessage("lang");
        $.get(chrome.extension.getURL("/setup/"+lang+"/bs.diaspora.html"))
            .success(function(data) {
                $value.html(data);
                $('[jsname="WsjYwc"]').first().append($value);
        });
    },
    
    Draw: function($ce) {
        if ($ce.find('.diasporaShare').length>0) return;
        var pod=(Subs.Settings.Values.Pod);
        if (!pod) return;
        icon=' <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAQAAAC1+jfqAAABkklEQVQoUwXBPUhUAQAA4O/de5737qfS89AoEsEhFIciiiYjCAohCNrbWtpLcM1ojaCgVYcGJyGoLcekP4IGQbKUrqQ0z5877873Xt8XDIBQr1I+nsyP0FltLrWStkSGiECPSlib7puOyyESrcbO87+zjf2uTDAgrzpy8mX1YlGmJVOQc2h7pX7jy1pNMKgvP/x+cKJXYNd9mXkNoY7N1fUL241crDbTPxFJpA6dc14olYlUR2sPC3Llof6ZXrFBW5qga1dLQeTE3XJ/rng9DhNjHnnqqg1k7pk3qSvOx9ei/JlQoIxTHoAnYBSh/EgUxIEebyw4645xNLywZktZW5CPuj8TobLAoXFw3Hf/HJPT1F3PNV+3pRKpWTwzh8dIpTpaS7n91Z2FtsQVFXVvLaqrmJLq2H11sBYMqdWGv1ZrbcNCGzitalnBTuPHpT8rYeyomX2ObpYKTQd6RPZsCjX267c/LReERanVb4W5bCw3SipxpGVr6detj+8oCgYQiJRULpemohrd383FvQ9NXRn+A1iPlomQUIqjAAAAAElFTkSuQmCC" style="border: 0px solid;display: inline-block;padding-bottom: 3px;" />';
        //diaspora*';
        var iconHtml='<div style="margin-left: 10px; cursor: pointer;" title="diaspora" role="button" data-tooltip="diaspora" data-tooltip-position="bottom" data-tooltip-vertical-offset="-12" data-tooltip-horizontal-offset="0" class="diasporaShare">[SVG]diaspora*</div> ';
        var html=iconHtml.replace("[SVG]",icon);

        var postarea=$ce.find(".jlbQfe");
        if (postarea.length==0) return;

        $(postarea).append(html);
    },
    ShareToDiaspora :function($post) {
       var text= $post.closest("c-wiz").find('[jsname="x0DHxb"]').text();
        var url= $($post.closest("c-wiz").find('.skMbkf')[0]).attr("href");
        var target= $post.closest("c-wiz").find('.ny6AZd').text();

        this.OpenShareWindow(target, text, url);
    },
    OpenShareWindow:function(target, content, url) {
            var pod=Subs.Settings.Values.Pod;
            var title=Subs.Settings.Values.Diasporatitle;
            var completeUrl='https://'+pod+'/bookmarklet?title='+encodeURIComponent(title)+'&notes='+encodeURIComponent(content);
            if (url) completeUrl+='&url='+encodeURIComponent(url);
            window.open(completeUrl,
            'das',
            'location=no,links=no,scrollbars=no,toolbar=no,width=620,height=550');
    },
    Dom: function ($ce) {
        this.Draw($ce);
    },
}